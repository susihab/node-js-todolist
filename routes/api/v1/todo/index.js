const express = require('express');

const router = express.Router();

const taskService = require('../../../../services/TaskService');

/**
 * @swagger
 * definitions:
 *   Task:
 *     type: object,
 *     required:
 *       - task
 *     properties:
 *       task:
 *         type: string
 *       duedate:
 *         type: string
 *       priority:
 *         type: number
 *       done:
 *         type: boolean
 *       user_id:
 *         type: string
 */

module.exports = (params) => {
  const logger = params.config.logger();

  /**
   * @swagger
   * /todo:
   *   post:
   *     parameters:
   *       - in: body
   *         name: task
   *         schema:
   *           $ref: "#/definitions/Task"
   *     description: Creates a new task
   *     security:
   *       - BearerAuth: []
   *     responses:
   *       201:
   *         description: Task created
   *       400:
   *         description: Missing Data
   *       500:
   *         description: General Error
   */
  router.post('/', (req, res) => {
    try {
      if (!req.body.task) {
        return res.status(400).json({ error: 'Missing task!' });
      }

      const newTask = taskService.add(req.body);
      return res.status(201).json({ data: newTask });
    } catch (error) {
      logger.fatal(error);
      return res.status(500).json({ error });
    }
  });

  return router;
};
