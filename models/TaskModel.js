const Mongoose = require('mongoose');

const TaskSchema = Mongoose.Schema({
  task: {
    type: String,
    required: true,
  },
  duedate: {
    type: Date,
    required: false,
  },
  priority: {
    type: Number,
    required: false,
    default: 1,
  },
  done: {
    type: Boolean,
    required: true,
    default: false,
  },
  user_id: {
    type: String,
    required: false,
  },
}, { timestamps: true });

module.exports = Mongoose.model('Task', TaskSchema);
