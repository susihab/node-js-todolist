const TaskModel = require('../models/TaskModel');

class TaskService {
  static async add(task) {
    const taskModel = new TaskModel(task);
    return taskModel;
  }
}

module.exports = TaskService;
