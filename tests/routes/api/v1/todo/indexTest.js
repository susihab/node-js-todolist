/* eslint-disable no-unused-expressions */
const chai = require('chai');
const chaiHttp = require('chai-http');

const TaskModel = require('../../../../../models/TaskModel');

chai.use(chaiHttp);
chai.should();

const { expect } = chai;
const config = require('../../../../../config').test;
const server = require('../../../../../app');

const app = server(config);

afterEach((done) => {
  TaskModel.remove().then(() => done());
});

const task = {
  task: 'hallo',
  user_id: 1,
};

describe('POST /api/v1/todo', () => {
  it('should return 400 for incomplete data', (done) => {
    chai.request(app)
      .post('/api/v1/todo')
      .send({ task: 'hallo', user_id: 1 })
      .end((err, res) => {
        res.should.have.status(400);
        expect(res).to.be.json;
        expect(res.error).to.exist;
        done();
      });
  });

  it('should return 201 and the newly created task with valid data', () =>
    chai.request(app)
      .post('/api/v1/todo')
      .send(task)
      .then((res) => {
        res.should.have.status(201);
        /* eslint-disable no-unused-expressions */
        expect(res).to.be.json;
        expect(res.body.data).to.exist;
        expect(res.body.data._id).to.exist;
      }));
});
